/**
 * module runTask
 *
 */
'use strict';
var Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	child_process = Promise.promisifyAll(require("child_process")),
	util = require("util");
module.exports = (ee,jasmine, taskFile)=>new Promise((resolve, reject)=> {
	ee.emit("theTask",taskFile);
	jasmine.onComplete(function(passed) {
		ee.emit("endTheTask",taskFile);
		resolve();
	});
	jasmine.execute([taskFile]);
})