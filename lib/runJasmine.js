/**
 * module runJasmine
 *
 */
var util = require("util"),
	Jasmine = require("jasmine");
function runJasmine(file, theReporter, saveData) {
	"use strict";
	let jasmine = new Jasmine();
	if (theReporter && theReporter !== 'null' && theReporter !== 'undefined') {
		let rep = require(theReporter);
		if (typeof rep === 'function') {
			jasmine.addReporter(rep());
		} else {
			jasmine.addReporter(rep);
		}

	}
	if (saveData === 'TRUE') {
		jasmine.addReporter(require(__dirname + "/serialReporter")());
	}
	jasmine.execute([file]);

}
module.exports = runJasmine.apply(null, process.argv.splice(2));