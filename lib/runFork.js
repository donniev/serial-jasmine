/**
 * module runFork
 *
 */
'use strict';
var Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	child_process = require("child_process"),
	util = require("util");

module.exports = (file, theReporter, saveData)=> new Promise((resolve, reject)=> {
	saveData = saveData ? "TRUE" : "FALSE";
	"use strict";
	let isResolved=false;
	let results = void(0)
	let child = child_process.fork(__dirname + "/runJasmine", [file, theReporter, saveData], {silent: false});
	child.on("exit", (signal)=> {
			if(! isResolved){
				resolve(results)
			}
		}
	);
	child.on("message", (messageObj)=> {
		switch (messageObj.type) {
			case "jasmineDone":
			{
				messageObj.message.file = file;
				results = messageObj.message;
				isResolved=true;
				resolve(results);
				//sometimes jasmine will not exit if test doesn't clear the event loop
				child.kill('SIGTERM');
				break;
			}
		}
	});
	child.on("error", (error)=>console.log(error));
});



